'use strict';

const userURL = 'https://ajax.test-danit.com/api/json/users';
const postURL = 'https://ajax.test-danit.com/api/json/posts';
const createOwnPost = document.getElementById('create-new-post-btn');


const loadPost = document.createElement('button');
loadPost.type = 'button';
loadPost.classList.add('load-btn')
loadPost.textContent = "LOAD POST";
document.body.append(loadPost);

//create post from url`s
function createPost(data){

const tweet = document.createElement('div')
tweet.classList.add('tweet');
createOwnPost.after(tweet);


const avatar = document.createElement('img');
avatar.src = "https://picsum.photos/200/" + data.id * 100;
avatar.classList.add('avatar');
tweet.append(avatar);

const content = document.createElement('div');
content.classList.add('content');
tweet.append(content);


const userHeader = document.createElement('div');
userHeader.classList.add("header");
content.append(userHeader);

const titleName = document.createElement('a');
titleName.href = '#';
titleName.classList.add('name');
titleName.textContent = data.name;
const userName = document.createElement('a');
userName.href = '#'
userName.classList.add('username');
userName.textContent = "@" + data.username;
content.append(titleName);
content.append(userName);

const postTitle = document.createElement('p');
postTitle.textContent = data.title;
postTitle.classList.add('post-title');
content.append(postTitle);

const post = document.createElement('p');
post.textContent = data.body;
content.append(post);

const actions = document.createElement('div');
actions.classList.add("actions");
content.append(actions);


const likeBtn = document.createElement('button');
likeBtn.classList.add("like-button");
likeBtn.textContent = "LIKE"
actions.append(likeBtn);


let likeCounter = 0; 
const likeCount = document.createElement('p');
likeCount.classList.add('like')
likeCount.textContent =  '♥ '  + likeCounter;
actions.append(likeCount);

likeBtn.addEventListener('click', () => {
  likeCounter++;
  likeCount.textContent = '♥ ' + likeCounter;
});

const retweetBtn = document.createElement('button');
retweetBtn.classList.add("retweet-button");
retweetBtn.textContent = "RETWEET"
actions.append(retweetBtn);


let retweetCounter = 0; 
const retweetCount = document.createElement('p');
retweetCount.classList.add('retweet')
retweetCount.textContent =  '🔁 '  + retweetCounter;
actions.append(retweetCount);

retweetBtn.addEventListener('click', () => {
  retweetCounter++;
  retweetCount.textContent = '🔁 ' + retweetCounter;
});



const deleteBtn = document.createElement('button');
deleteBtn.classList.add("delete-button");
deleteBtn.setAttribute("data-post-id", data.id);
deleteBtn.textContent = "DELETE"
actions.append(deleteBtn);

deleteBtn.addEventListener('click',  async ()=>{
      const postId = deleteBtn.getAttribute("data-post-id");
    try {
      const response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE'
      });
      if (response.ok) {
        tweet.remove();
      } else {
        console.error('cant delete');
      }
    } catch (error) {
      console.error('error:', error);
    }
});

}





//CLASS CARD
class Card {
    constructor(name, username, id, title, body, userId){
        this.name = name;
        this.username = username;
        this.id = id;
        this.title = title;
        this.body = body;
        this.userId = userId;
    }
}

// GET USER
async function getUser(url){
    const USERresponse = await fetch(url, {
        method: 'GET'
    });
    const dataUser = await USERresponse.json();
    return dataUser;
}


// GET POST
async function getPost(url){
    const POSTresponse = await fetch(url, {
        method: 'GET'
    });
    const dataPost = await POSTresponse.json();
    return dataPost;
    
}


//get all result to array
async function loadAllData(){
    let result = await Promise.all([getUser(userURL), getPost(postURL)])  ;
    return result;
  }



// animation + add post to html

loadPost.addEventListener('click', ()=>{
  loadPost.style.display = 'none';
createOwnPost.style.display = 'block';

  const loader = document.createElement('div');
  document.body.append(loader);
  loader.classList.add('loader');

  setTimeout(()=>{

  loader.style.display = 'none';

  loadAllData().then(([users, posts]) => {
    for (let { id, name, username } of users) {
        let postCreated = false; 
        for (let { title, body, userId } of posts) {
        if (id === userId && !postCreated) { 
            let data = new Card(name, username, id, title, body);
            createPost(data);
            postCreated = true;
        }
        }
    }
    });
}, 2500)

})




// create OWN post

const modal = document.getElementById("myModal");
const closeModalBtn = document.getElementsByClassName("close")[0];
const createPostButton = document.getElementById("createPost");

createOwnPost.addEventListener("click", () => {
  modal.style.display = "block";
});

closeModalBtn.addEventListener("click", () => {
  modal.style.display = "none";
});

window.addEventListener("click", (event) => {
  if (event.target === modal) {
    modal.style.display = "none";
  }
});

createPostButton.addEventListener("click", () => {
  const name = document.getElementById("name").value;
  const username = document.getElementById("username").value;
  const title = document.getElementById("title").value;
  const body = document.getElementById("body").value;
  let userId = 1;
  
  const newPost = new Card (name, username, userId, title, body);
  createPost(newPost);
  modal.style.display = "none";

});
